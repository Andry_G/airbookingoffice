package com.bionic.edu.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.faces.application.FacesMessage;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;

import com.bionic.edu.entity.Ordering;
import com.bionic.edu.entity.Ordering.StatusOrder;
import com.bionic.edu.entity.Ticket;
import com.bionic.edu.service.AccountantService;

@Named
@Scope("session")
public class OrdersBean implements Serializable {

	@Inject
	private AccountantService accountantService;
	
	private static final long serialVersionUI = 3L;
	private List<Ordering> list = new ArrayList<>();
	
	public OrdersBean() {
		
	}
	
	public void init() {
		list = accountantService.showBookedOrdering();
	}
	
	public void paidOrder(String idString) {
		int id = Integer.valueOf(idString);
		accountantService.changeStatusOrder(id);
		
		String mess = "The status of order № " + id + " was changed";	
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Change completed", mess);
        RequestContext.getCurrentInstance().showMessageInDialog(message);
	}
	
	public double amountOrder(Ordering ordering) {
		List<Ticket> listTickets =  (List<Ticket>) ordering.getTickets();
		double amount = 0d;
		for(Ticket ticket : listTickets) {
			amount += ticket.getPrice();
		}
		return amount;
	}
	
	public List<Ordering> getList() {
		return list;
	}
	
	public void setList(List<Ordering> list) {
		this.list = list;
	}
}
